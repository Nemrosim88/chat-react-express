// =========================================================
// =================  MODULES  =============================
const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const moment = require('moment');
moment().format();
const path = require("path");
const socketIO = require('socket.io');
const http = require('http');
require('dotenv').config();

const commands = require('./server/db');



// =========================================================
// ================  FIREBASE  =============================
const firebase = require("firebase/app");
require("firebase/database");

// =========================================================
// =====================  CONFIG  ==========================
const app = express();
app.use(bodyParser.json());
app.use(cors());  //Simple Usage (Enable All CORS Requests)

// =================HEROKU================
if (process.env.NODE_ENV === 'production') { // variant 2
    app.use(express.static('client/build'));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

// app.use(express.static(path.join(__dirname, "client/build")));
// app.get("*", (req, res) => {
//     res.sendFile(path.join(__dirname + "/client/build/index.html"));
// });
// ========================================

const env = process.env;
const config = {
    apiKey: env.API_KEY,
    authDomain: env.AUTH_DOMAIN,
    databaseURL: env.DATABASE_URL,
    projectId: env.PROJECT_ID,
    storageBucket: env.STORAGE_BUCKET,
    messagingSenderId: env.MESSAGING_SENDER_ID
};
firebase.initializeApp(config);


// =========================================================
// =====================  SOCKET  ==========================
const server = http.createServer(app);
const socketForCreateMessage = require('./server/io/clientCreateMessage');

const io = socketIO(server);
io.on('connection', (socket) => {   // register event listener. 'connection' -> client CONNECTED TO THE SERVER
    console.log("New user connected", new Date());
    socket.on('disconnect', (socket) => {   // register event listener. 'connection' -> client CONNECTED TO THE SERVER
        console.log("User was disconnected", new Date());
    });


    socketForCreateMessage(io, socket, firebase).then((result) => {
        console.log("newMessage saved in firebase:");
    }).catch((error) => {
        console.log("newMessage not saved in firebase:");
    });


    // socket.on('newMessage', (message) => {
    //     console.log('emitFromClient', message);
    // });
});


// =========================================================
// =====================  APP  =============================


app.post('/login', (req, res) => {
    require(commands.LOGIN)(firebase, req)
        .then((result) => {
            res.send(result);
        }).catch(error => {
        res.send(error);
    });
});


app.post('/logout', (req, res) => {
    require('./server/db/commands/auth/logout')(firebase, req).then((result) => {
        res.send(result);
    }).catch(error => {
        res.send({error: error});
    });
});

app.post('/addUser', (req, res) => {
    require('./server/db/commands/addUser')(firebase, req).then((result) => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });
});


app.post('/messages', (req, res) => {
    require('./server/db/commands/messages/getMessages')(firebase, req).then((result) => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });
});


const PORT = process.env.PORT || 5000;
app.set("port", PORT);
server.listen(PORT, () => {
    console.log(`Mixing it up on port ${PORT}`);
    console.log("process.env.NODE_ENV = ", process.env.NODE_ENV)
});

