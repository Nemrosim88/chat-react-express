# Project Info
* npm run start
* cd client
* npm run start

## Adding heroku
* after creating git init, add, commit
* heroku create
* You can use the git remote command to confirm that a 
remote named heroku has been set for your app:
* git remote -v
* git push heroku master

##### ВАЖНО:
EXPRESS + REACT + HEROKU
добавить в package - "heroku-postbuild": "cd client && npm install && npm run build"


Для Redux: (/client)
npm install --save redux
npm install --save react-redux   ==> позволяет прикрепить redux store к приложению
npm install --save-dev redux-devtools


## FIREBASE
Enable the Google sign-in provider in the Authentication > SIGN-IN METHOD tab.
You must have the Firebase CLI installed.
 If you don't have it install it with <b>npm install -g firebase-tools</b> and then configure it with firebase login.
On the command line run firebase use --add and select the Firebase project you have created.
On the command line run firebase serve using the Firebase CLI tool to launch a local server


const state = Object.assign({}, state); -> клонирование State (или любого другого объекта)
Замена этому:
...state, -> spread operator

2. Вместо state.array.push() лучше использовать state.array.concat().

3. Type of State:
Local UI State (show/hide)
Persistent State (users,posts)
ClientState (logged, filters)

4. functionName: () => dispatch({ type: types.SOME_ARRAY })

или через action creator:

import {someFunction} from './action'
functionName: () => dispatch(someFunction())

REDUX_SAGA
1. y delay(1000)
2. put ({type:""ACTION_NAME}) -> Effect


## ERRORS:
Error: ENOENT: no such file or directory, scandir 'C:\Users\Nemrosim\Desktop\PROJECTS\map-coordinates-react-node\client\node_modules\node-sass\vendor'
Solution: run npm rebuild node-sass




