module.exports = (io, socket, firebase) => new Promise((resolve, reject) => {

    socket.on('clientCreateMessage', (message) => {
        require('../db/commands/messages/clientCreateMessage')(firebase, message).then((result) => {
            io.emit("newMessage", message);
            resolve(result);
        }).catch(error => {
            reject(error);
        });
    });
});