const jwt = require('jsonwebtoken');
const moment = require('moment');
moment().format();

module.exports = (firebase, req) => new Promise((resolve, reject) => {

    const {email, password, nickname, gender, photo} = req.body;
    const timestamp = moment().unix();
    const utcOffset = moment().utcOffset();

    const currentDate = new Date();

    let hashedPassword = jwt.sign(password, require('../../salt'));

    firebase.database().ref('/users').push({
        registrationDate: currentDate,
        registrationDateTimeStamp: timestamp,
        registrationDateUTCOffset: utcOffset,
        email: email,
        password: hashedPassword,
        nickname: nickname,
        gender: gender,
        photo: photo,
        tokens: ["one", "two"]
        // profile_picture : imageUrl
    }).then(result => {
        resolve(result);
    }).catch(error => {
        console.log(error);
        reject(error);
    });

});