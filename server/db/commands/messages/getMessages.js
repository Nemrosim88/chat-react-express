module.exports = (firebase, req) => new Promise((resolve, reject) => {

    const query = firebase.database().ref("/messages").orderByChild("sendTimeStamp");


    query.once("value").then(result => {
        let generalMessages = [];
        let randomMessages = [];
        let frontendMessages = [];
        let todayLearnedMessages = [];

        result.forEach(child => {

            /*
            CHILD:  {
            channel: 'todayLearned',
            from: 'Nemrosim',
            sendTimeStamp: 1543926915,
            sendUTCOffset: 120,
            text: 'asdasd' }
             */
            const message = child.val();

            if (message.channel === "general") {
                generalMessages.push(message);
            }
            if (message.channel === "random") {
                randomMessages.push(message);
            }
            if (message.channel === "frontendTeam") {
                frontendMessages.push(message);
            }
            if (message.channel === "todayLearned") {
                todayLearnedMessages.push(message);
            }
        });

        const data = {
            generalMessages: generalMessages,
            generalMessagesLength: generalMessages.length,
            randomMessages:randomMessages,
            randomMessagesLength:randomMessages.length,
            frontendMessages:frontendMessages,
            frontendMessagesLength:frontendMessages.length,
            todayLearnedMessages:todayLearnedMessages,
            todayLearnedMessagesLength:todayLearnedMessages.length
        };

        resolve(data);


    })
        .catch(error => {
            reject(error);
        });
});