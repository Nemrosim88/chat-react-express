const moment = require('moment');
moment().format();

module.exports = (firebase, message) => new Promise((resolve, reject) => {

    const {from, channel, text} = message;

    const timestamp = moment().unix();
    const utcOffset = moment().utcOffset();

    firebase.database().ref('/messages').push({
        sendTimeStamp: timestamp,
        sendUTCOffset: utcOffset,
        from: from,
        channel: channel,
        text: text,
    }).then(result => {
        console.log("[clientCreateMessage.js] RESULT: ", result);
        resolve(result);
    }).catch(error => {
        console.log("[clientCreateMessage.js] ERROR: ", error);
        reject(error);
    });
});