module.exports = (firebase, req) => new Promise((resolve, reject) => {

    const {token, tokenKey} = req.body;

    const ref = firebase.database().ref("tokens/" + tokenKey);
    const promise = ref.remove();

    promise.then((result) => {
            resolve(result);
        }
    ).catch((error) => {
        reject({error});
    })
});