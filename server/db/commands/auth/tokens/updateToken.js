const jwt = require('jsonwebtoken');

module.exports = (firebase, email) => new Promise((resolve, reject) => {

    const token = {
        lastLogin: new Date(),
        email: email
    };

    const result = jwt.sign(token, require('../../../../salt'));

    var adaNameRef = firebase.database().ref('users/tokens').update({token: 'Ada', last: 'Lovelace'});


    query
        .once("value")
        .then(result => {
            let user = {
                email: null,
                childKey: null,
                nickname: null,
            };

            let hashedPassword = null;
            result.forEach(child => {
                // user.email = child.val().credentials.email;
                user.childKey = child.key;
                user.email = child.val().email;
                hashedPassword = child.val().password;
                user.nickname = child.val().nickname;
            });

            const unhashedPassword = jwt.verify(hashedPassword, require('../../../../salt'));

            if (user.email === null) {
                resolve({status: "Wrong email"});
            } else if (unhashedPassword !== password) {
                resolve({status: "Wrong password"});
            } else {
                resolve(user);
            }
        })
        .catch(error => {
            reject(error);
        });
});