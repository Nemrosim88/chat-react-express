const jwt = require('jsonwebtoken');
const moment = require('moment');
moment().format();

module.exports = (firebase, email) => new Promise((resolve, reject) => {

    const timestamp = moment().unix();
    const utcOffset = moment().utcOffset();

    const tokenData = {
        timestamp: timestamp,
        utcOffset: utcOffset,
        email: email
    };

    const salt = require('../../../../salt');
    const jwtResult = jwt.sign(tokenData, salt);


    const key = firebase.database().ref('tokens').push({token: jwtResult, email: email}, (error) => {
        if (error) {
            reject({error: "Error in creating new token"});
        } else {
            resolve({tokenKey: key, token: jwtResult});
        }
    }).key;


});