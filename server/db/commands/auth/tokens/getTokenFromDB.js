const jwt = require('jsonwebtoken');

module.exports = (firebase, token) => new Promise((resolve, reject) => {

    const database = firebase.database().ref("/users");
    const query = database.orderByChild("email").equalTo(email);

    query.once("value").then(result => {
        let user = {
            email: null,
            childKey: null,
            nickname: null,
            token: null,
            tokenKey: null
        };

        let hashedPassword = null;
        result.forEach(child => {
            // user.email = child.val().credentials.email;
            user.childKey = child.key;
            user.email = child.val().email;
            hashedPassword = child.val().password;
            user.nickname = child.val().nickname;
        });


        if (user.email !== email) {
            reject({error: "Wrong email"});
        }

        const unhashedPassword = jwt.verify(hashedPassword, require('../../../salt'));
        if (unhashedPassword !== password) {
            console.log("WRONG PASSWORD");
            reject({error: "Wrong password"});
        } else {





            require('../auth/tokens/createNewToken')(firebase, user.email).then(({token, tokenKey}) => {
                user.token = token;
                user.tokenKey = tokenKey;
                resolve(user);
            }).catch(error => {
                reject({status: error})
            });
        }
    })
        .catch(error => {
            reject(error);
        });
});