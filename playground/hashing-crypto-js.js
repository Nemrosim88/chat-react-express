const {SHA256} = require('crypto-js');


var message = 'message';
var sha256 = SHA256(message).toString();
console.log("Message ", message);
console.log("SHA ", sha256);

var data = {
    id: 4,
};

var token = {
    data,
    hash: SHA256(JSON.stringify(data) + 'someSecret').toString(),
};

var resultHash = SHA256(JSON.stringify(token.data)+'someSecret').toString();

console.log("EQUAL: ", token.hash===resultHash);