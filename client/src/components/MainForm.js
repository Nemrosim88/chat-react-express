import React, {Component} from "react";
import {Route, BrowserRouter as Router, withRouter,} from "react-router-dom";
import {connect} from "react-redux";
import {PropTypes} from 'prop-types'

import {
    Container,
    Col,
    ListGroup,
} from 'reactstrap';
import {SideBar} from "../routes/SideBar";
import {Channel} from '../routes/Channel';


// If the client is served on the same domain as the server you can simply do
//
//     var socket = io();
// const socket = io('http://localhost:5000');



class MainForm extends Component {

    render() {

        return (
            <Router>
                <Container className="main">
                    <Col xs="3" className="left">
                        <ListGroup>
                           <SideBar/>
                        </ListGroup>
                    </Col>

                    <Col className="right">

                        {/*WAS*/}
                        {/*{ChannelsRoutes.map((route, index) => (*/}
                            {/*<Route*/}
                                {/*key={index}*/}
                                {/*path={route.path}*/}
                                {/*exact={route.exact}*/}
                                {/*component={route.component}*/}
                            {/*/>*/}
                        {/*))}*/}

                        <Route path="/:id" component={Channel} />
                    </Col>
                </Container>

            </Router>

        );
    }
}

const MainFormContainer = withRouter(connect(({auth}) => ({
        logging: auth.logging,
        logged: auth.logged,
        nickname: auth.nickname,
    }),
    {}
)(MainForm));

export {MainFormContainer as MainForm};
