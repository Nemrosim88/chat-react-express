import React, {Component} from "react";
import {Container, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText} from "reactstrap";

class RandomScreen extends Component {

    render() {
        return (
            <Container>
                <ListGroup>
                    <ListGroupItem active>
                        <ListGroupItemHeading>01.</ListGroupItemHeading>
                        <ListGroupItemText>
                            Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
                        </ListGroupItemText>
                    </ListGroupItem>
                    <ListGroupItem>
                        <ListGroupItemHeading>List group item heading</ListGroupItemHeading>
                        <ListGroupItemText>
                            Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
                        </ListGroupItemText>
                    </ListGroupItem>
                    <ListGroupItem>
                        <ListGroupItemHeading>List group item heading</ListGroupItemHeading>
                        <ListGroupItemText>
                            Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
                        </ListGroupItemText>
                    </ListGroupItem>
                </ListGroup>
            </Container>
        );
    }
}

export {RandomScreen};
