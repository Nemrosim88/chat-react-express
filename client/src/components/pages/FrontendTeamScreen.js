import React, {Component} from "react";
import {Container, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText} from "reactstrap";

class FrontendTeamScreen extends Component {
    render() {
        return (
            <Container>
            <ListGroup flush>
                <ListGroupItem>
                    <ListGroupItemHeading>01.</ListGroupItemHeading>
                    <ListGroupItemText>
                        Какое-то сообщение.
                    </ListGroupItemText>
                </ListGroupItem>
                <ListGroupItem>
                    <ListGroupItemHeading>List group item heading</ListGroupItemHeading>
                    <ListGroupItemText>
                        Ещё одно сообщение.
                    </ListGroupItemText>
                </ListGroupItem>
                <ListGroupItem>
                    <ListGroupItemHeading>List group item heading</ListGroupItemHeading>
                    <ListGroupItemText>
                        Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.
                    </ListGroupItemText>
                </ListGroupItem>
            </ListGroup>
            </Container>
        );
    }
}

export {FrontendTeamScreen};
