import React, {Component} from "react";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';


class NoInputDataModal extends Component {

    state = {
        modal: false,
        nestedModal: false,
        closeAll: false
    };

    render() {
        const {toggle, isOpen, text} = this.props;
        return (
            <div>
                <Modal isOpen={isOpen} toggle={toggle}>
                    <ModalHeader toggle={toggle}>Warning</ModalHeader>
                    <ModalBody>
                        {text}
                    </ModalBody>
                    <ModalFooter>
                        {/*<Button color="primary" onClick={toggle}>Forgot password</Button>*/}
                        <Button block={true} color="secondary" onClick={toggle}>OK</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}


export default NoInputDataModal;