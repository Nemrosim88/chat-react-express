import React, {Component} from "react";
import {connect} from "react-redux";
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
} from 'reactstrap';
import isEmail from 'validator/lib/isEmail';


import {AddUserForm} from "./AddUserForm";
import i18n from '../../../i18n';
import {PacmanLoader} from "react-spinners";
import * as UserActions from "../../../store/actions/userActions";


class AddNewUserModal extends Component {

    state = {
        email: '',
        password: '',
        secondPassword: 'second',
        nickname: '',
        gender: true, // true = male
        photo: null,

        passwordsDontMatch: false,
        invalidEmail: false,
        noNickname: false,
    };

    inputChanged = (key, value) => {
        this.setState({
            [key]: value
        })
    };

    validationPassed = () => {
        const {email, password, secondPassword, nickname} = this.state;

        if (!isEmail(email)) {
            this.setState({invalidEmail: true});
            return false;
        } else {
            this.setState({invalidEmail: false});
        }

        if (password !== secondPassword) {
            this.setState({passwordsDontMatch: true});
            return false;
        } else {
            this.setState({passwordsDontMatch: false});
        }

        if (nickname === '') {
            this.setState({noNickname: true});
            return false;
        } else {
            this.setState({noNickname: false});
        }

        return true;
    };

    addUserButtonHandler = () => {
        if (this.validationPassed()) {
            const {email, password, nickname, gender, photo} = this.state;

            this.props.saga_addUser({
                email: email,
                password: password,
                nickname: nickname,
                gender: gender,
                photo: photo,

            });
        }
    };

    // TODO:Загрузка-фотографий
    render() {
        const {toggle, modal, downloading} = this.props;
        const {invalidEmail, passwordsDontMatch, noNickname} = this.state;
        return (
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>{i18n.t('addUser__registrationForm')}</ModalHeader>
                <ModalBody>
                    {downloading ? <PacmanLoader/> : <AddUserForm
                        inputChanged={this.inputChanged}
                        invalidEmail={invalidEmail}
                        passwordsDontMatch={passwordsDontMatch}
                        noNickname={noNickname}/>}
                </ModalBody>
                <ModalFooter>
                    <Button block color="primary"
                            onClick={this.addUserButtonHandler}>{i18n.t('addUser__registrationButton')}</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

const ComponentContainer = connect(({user}) => (
        {downloading: user.downloading}  //state
    ),
    {saga_addUser: UserActions.action_addUserRequest} //action
)(AddNewUserModal);

export {ComponentContainer as AddNewUserModal};