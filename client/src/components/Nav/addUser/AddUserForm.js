import React, {Component} from "react";
import {connect} from "react-redux";

import {
    Col,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Row,
    FormFeedback
} from 'reactstrap';


import * as UserActions from '../../../store/actions/userActions';
import i18n from '../../../i18n';

class AddUserForm extends Component {

    render() {
        const {inputChanged, invalidEmail, passwordsDontMatch, noNickname} = this.props;
        return (
            <Form>
                <FormGroup row>
                    <Label for="newUserEmail" sm={2}>Email</Label>
                    <Col sm={10}>
                        <Input type="email"
                               name="email"
                               id="newUserEmail"
                               placeholder={i18n.t('addUser__email_placeholder')}
                               invalid={invalidEmail}
                               onChange={(e) => {
                                   inputChanged("email", e.target.value)
                               }}/>
                        {invalidEmail ? <FormFeedback>Not an email</FormFeedback> : null}
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="newUserPassword" sm={2}>Password</Label>
                    <Col sm={10}>
                        <Input type="password"
                               name="password"
                               id="newUserPassword"
                               placeholder={i18n.t('addUser__password_placeholder')}
                               invalid={passwordsDontMatch}
                               onChange={(e) => {
                                   inputChanged("password", e.target.value)
                               }}/>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="newUserSecondPassword" sm={2}>Password</Label>
                    <Col sm={10}>
                        <Input type="password"
                               name="password"
                               id="newUserSecondPassword"
                               placeholder={i18n.t('addUser__secondPassword_placeholder')}
                               invalid={passwordsDontMatch}
                               onChange={(e) => {
                                   inputChanged("secondPassword", e.target.value)
                               }}/>
                        {passwordsDontMatch ? <FormFeedback>Passwords don't match</FormFeedback> : null}
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="newUserNickname" sm={2}>Password</Label>
                    <Col sm={10}>
                        <Input type="text"
                               name="nickname"
                               id="newUserNickname"
                               placeholder={i18n.t('addUser__nickname_placeholder')}
                               invalid={noNickname}
                               onChange={(e) => {
                                   inputChanged("nickname", e.target.value)
                               }}/>
                        {noNickname ? <FormFeedback>Please enter nickname</FormFeedback> : null}

                    </Col>
                </FormGroup>


                <FormGroup row>
                    <Label for="exampleFile" sm={2}>File</Label>
                    <Col sm={10}>
                        <Input type="file" name="file" id="exampleFile"/>
                        <FormText color="muted">
                            {i18n.t('addUser__inDevelopment')}
                        </FormText>
                    </Col>
                </FormGroup>

                <FormGroup tag="fieldset">
                    <legend>{i18n.t('addUser__sex_placeholder')}</legend>
                    <Row>
                        <Col>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio"
                                           name="radio1"
                                           checked={true}
                                           onChange={() => inputChanged('gender', true)}/>{' '}
                                    {i18n.t('addUser__sex_male')}
                                </Label>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio"
                                           name="radio1"
                                           onChange={() => inputChanged('gender', false)}/>{' '}
                                    {i18n.t('addUser__sex_female')}
                                </Label>
                            </FormGroup>
                        </Col>
                    </Row>
                </FormGroup>
            </Form>
        );
    }
}

const ComponentContainer = connect(({user}) => (
        {}), //state
    {saga_addUser: UserActions.action_addUserRequest} //action
)(AddUserForm);

export {ComponentContainer as AddUserForm};