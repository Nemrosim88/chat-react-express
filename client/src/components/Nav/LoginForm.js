import React, {Component} from "react";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {Row, Col, FormGroup, Input, Button, Form} from 'reactstrap'

import * as AuthActions from '../../store/actions/authActions';
import NoInputDataModal from "./NoInputDataModal";
import {AddNewUserModal} from "./addUser/AddNewUserModal";


class LoginForm extends Component {

    state = {
        email: null,
        password: null,
        newUserModal: false,
        emailInputModal: false,
        passwordInputModal: false,

        emailErrorModal: false,
        passwordErrorModal: false,

        registrationModal: false,
        closeAll: false

    };

    inputChanged = (key, value) => {
        this.setState({[key]: value})
    };

    // toggle = () => {
    //     this.setState({
    //         newUserModal: !this.state.newUserModal
    //     });
    // };

    emailInputToggle = () => {
        this.setState({emailInputModal: !this.state.emailInputModal});
    };

    passwordInputToggle = () => {
        this.setState({passwordInputModal: !this.state.passwordInputModal});
    };

    emailErrorToggle = () => {
        this.setState({emailErrorModal: !this.state.emailErrorModal});
    };

    passwordErrorToggle = () => {
        this.setState({passwordErrorModal: !this.state.passwordErrorModal});
    };

    toggleRegistrationModal = () => {
        this.setState({
            registrationModal: !this.state.registrationModal,
        });
    };


    loginButtonHandler = () => {
        console.log("process.env.baseURL = ", process.env.baseURL)
        const {email, password} = this.state;

        if (email === null) {
            this.emailInputToggle();
        } else if (password === null) {
            this.passwordInputToggle();
        } else {
            console.log('in method');
            this.props.saga_login({
                email: email,
                password: password,
            })
        }
    };


    componentDidMount() {
        const {error} = this.props;

        if (error === "Wrong email") {
            this.emailErrorToggle();
        }

        if (error === "Wrong password") {
            this.passwordErrorToggle();
        }
    }

    render() {

        const {
            emailInputModal,
            passwordInputModal,
            registrationModal,
            emailErrorModal,
            passwordErrorModal,
        } = this.state;


        return (
            <Form>
                <AddNewUserModal
                    toggle={this.toggleRegistrationModal}
                    modal={registrationModal}/>
                <Row form>
                    <Col md={4}>
                        <FormGroup>
                            <Input type="email"
                                   name="email"
                                   id="exampleEmail"
                                   placeholder="Email"
                                   onChange={(event) => this.inputChanged('email', event.target.value)}/>
                        </FormGroup>
                    </Col>
                    <Col md={4}>
                        <FormGroup>
                            <Input type="password"
                                   name="password"
                                   id="examplePassword"
                                   placeholder="Password"
                                   onChange={(event) => this.inputChanged('password', event.target.value)}/>
                        </FormGroup>
                    </Col>
                    <Col md={2}>
                        <Button onClick={this.loginButtonHandler}>Sign in</Button>
                    </Col>
                    <Col md={2}>
                        <Button onClick={this.toggleRegistrationModal}>Register</Button>
                    </Col>
                </Row>


                <NoInputDataModal
                    toggle={this.emailInputToggle}
                    text={"Please enter email"}
                    isOpen={emailInputModal}/>
                <NoInputDataModal
                    toggle={this.passwordInputToggle}
                    text={"Please enter password"}
                    isOpen={passwordInputModal}/>
                <NoInputDataModal
                    toggle={this.emailErrorToggle}
                    text={"Entered email did not match"}
                    isOpen={emailErrorModal}/>
                <NoInputDataModal
                    toggle={this.passwordErrorToggle}
                    text={"Entered password did not match"}
                    isOpen={passwordErrorModal}/>
            </Form>
        );
    }
}

LoginForm.propTypes = {
    logged: PropTypes.bool,
    // logged: PropTypes.func,
};

const ComponentContainer = connect(({auth}) => (
        //state
        {
            logged: auth.logging,
            email: auth.logging,
            token: auth.logging,
            error: auth.error,
        }
    ),
    //action
    {saga_login: AuthActions.action_loginRequest}
)(LoginForm);

export {ComponentContainer as LoginForm};