import React, {Component} from "react";
import {connect} from "react-redux";

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    Button, Container,
} from 'reactstrap';
import {LoginForm} from './LoginForm'
import {PropagateLoader} from "react-spinners";
import * as AuthActions from "../../store/actions/authActions";


class AppNav extends Component {

    state = {
        isOpen: false,
        email: null,
        password: null,
    };

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    };

    logoutButtonHandler = () => {
        const {token, tokenKey} = this.props;

        console.log("TOKEN AND TOKENKey: ", token, tokenKey);
        this.props.saga_logout({
            token: token,
            tokenKey: tokenKey,
        })
    };


    render() {
        const {logging, logged} = this.props;
        const loginForm = logging ? <Container><PropagateLoader/></Container> : <LoginForm/>;
        const loggedForm = <Button outline color="warning" onClick={this.logoutButtonHandler}>Logout</Button>;

        const navBarForm = logged ? loggedForm : loginForm;

        return (
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">ChatApp</NavbarBrand>
                <NavbarToggler onClick={this.toggle}/>
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        {navBarForm}
                    </Nav>
                </Collapse>
            </Navbar>

        );
    }
}


const AppNavContainer = connect(({auth}) => ({
        logged: auth.logged,
        logging: auth.logging,
        token: auth.token,
        tokenKey: auth.tokenKey
    }),
    {saga_logout: AuthActions.action_logoutRequest}
)(AppNav);

export {AppNavContainer as AppNav};
