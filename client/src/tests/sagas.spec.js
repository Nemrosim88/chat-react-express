import test from 'tape';

import { put, call } from 'redux-saga/effects'

import * as actions from '../store/actions/actions'
import { showMarkersChangedAsync, delay } from '../store/saga/sagas';

test("showMarkersChangedAsync Saga test", assert => { 
  const gen = showMarkersChangedAsync();

  assert.deepEqual(
    gen.next().value,
    call(delay, 2000),
    "incrementAsync Saga must call delay(2000)"
  );

  assert.deepEqual(
    gen.next().value,
    put({ type: actions.SHOW_MARKERS_CHANGED_ASYNC }),
    "incrementAsync Saga must dispatch an SHOW_MARKERS_CHANGED_ASYNC action"
  );

  assert.deepEqual(
    gen.next(),
    { done: true, value: undefined },
    "incrementAsync Saga must be done"
  );

  assert.end();
});
