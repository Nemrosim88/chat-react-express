import axios from 'axios';

import {AuthGateway, MessageGateway, UserGateway} from "./gateways";
import {AuthService, MessageService, UserService} from "./services";

const baseURL = process.env.LOCALHOST;

const restAdapter = axios.create({
    baseURL: baseURL
});

const fileAdapter = axios.create({
    baseURL: baseURL
});

const adapters = {
    restAdapter,
    fileAdapter
};

const authGateway = new AuthGateway(adapters);
const messageGateway = new MessageGateway(adapters);
const userGateway = new UserGateway(adapters);

const gateways = {
    authGateway,
    messageGateway,
    userGateway
};

const authService = new AuthService(gateways);
const messageService = new MessageService(gateways);
const userService = new UserService(gateways);

export {
    authService,
    messageService,
    userService
};

// export function applyAccessToken(accessToken) {
//     restAdapter.defaults.headers['X-Session-Id'] = fileAdapter.defaults.headers['X-Session-Id'] = accessToken;
//
//     fileAdapter.defaults.headers.post['Content-Type'] = 'multipart/form-data';
// }