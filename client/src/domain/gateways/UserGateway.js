import * as APIPaths from "../APIPaths";

class UserGateway {
    constructor({restAdapter}) {
        this.restAdapter = restAdapter;
    }

    async addUserGateway({email, password, nickname, gender, photo}) {
        const {data} = await this.restAdapter.post(APIPaths.ADD_USER, {email, password, nickname, gender, photo});
        return data;
    }
}

export {UserGateway};