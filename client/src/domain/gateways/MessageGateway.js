import * as APIPaths from "../APIPaths";

class MessageGateway {
    constructor({restAdapter}) {
        this.restAdapter = restAdapter;
    }


    async getMessagesRequest() {
        const {data} = await this.restAdapter.post(APIPaths.MESSAGES);
        return data;
    }
}

export {MessageGateway};