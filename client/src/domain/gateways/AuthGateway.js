import * as APIPaths from "../APIPaths";

class AuthGateway {
    constructor({restAdapter}) {
        this.restAdapter = restAdapter;
    }

    async login({email, password}) {
        const {data} = await this.restAdapter.post(APIPaths.LOGIN, {email, password});
        return data;
    }

    async logoutGateway({token, tokenKey}) {
        const {data} = await this.restAdapter.post(APIPaths.LOGOUT, {token, tokenKey});
        return data;
    }
}

export {AuthGateway};