class MessageService {
    constructor({messageGateway}) {
        this.messageGateway = messageGateway;
    }

    getMessagesRequest() {
        return this.messageGateway.getMessagesRequest();
    }
}

export {MessageService};