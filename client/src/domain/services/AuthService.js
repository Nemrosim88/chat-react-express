class AuthService {
    constructor({authGateway}) {
        this.authGateway = authGateway;
    }

    login({email, password}) {
        return this.authGateway.login({email, password});
    }

    logoutService({token, tokenKey}) {
        return this.authGateway.logoutGateway({token, tokenKey});
    }
}

export {AuthService};