class UserService {
    constructor({userGateway}) {
        this.userGateway = userGateway;
    }

    addUserService({email, password, nickname, gender, photo}) {
        return this.userGateway.addUserGateway({email, password, nickname, gender, photo});
    }
}

export {UserService};