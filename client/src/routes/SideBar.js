import React, {Component} from "react";
import {Badge, ListGroup, ListGroupItem} from "reactstrap";
import {NavLink, withRouter} from "react-router-dom";

import {Routes} from "./Routes";
import connect from "react-redux/es/connect/connect";
import * as MessageActions from "../store/actions/messageActions";

class SideBar extends Component {

    state = {
        generalMessagesLength: null,
        randomMessagesLength: null,
        frontendMessagesLength: null,
        todayLearnedMessagesLength: null,
    };

    componentDidMount() {
        const items = JSON.parse(localStorage.getItem('messagesByChannels'));
        this.props.saga_getMessages();

        if (items !== null) {

            const messages = this.props.redux_messages;


            this.setState({
                generalMessagesLength: (messages.generalMessagesLength - items.generalChannel.length),
                randomMessagesLength: messages.randomMessagesLength - items.randomChannel.length,
                frontendMessagesLength: messages.frontendMessagesLength - items.frontedTeam.length,
                todayLearnedMessagesLength: messages.todayLearnedMessagesLength- items.todayLearner.length,
            });

        } else {


            const messages = this.props.redux_messages;
            this.setState({
                generalMessagesLength: messages.generalMessagesLength,
                randomMessagesLength: messages.randomMessagesLength,
                frontendMessagesLength: messages.frontendMessagesLength,
                todayLearnedMessagesLength: messages.todayLearnedMessagesLength,
            });
        }
    }

    render() {

        const {generalMessagesLength, randomMessagesLength, frontendMessagesLength, todayLearnedMessagesLength} = this.state;
        return (
            <ListGroup>
                <ListGroupItem className="justify-content-between">
                    <NavLink exact to={Routes.GENERAL}>#general</NavLink>

                    {generalMessagesLength !== null ?
                        <Badge pill>{generalMessagesLength}</Badge> : null}

                </ListGroupItem>
                <ListGroupItem className="justify-content-between">
                    <NavLink exact to={Routes.RANDOM}>#random</NavLink>

                    {randomMessagesLength !== null ?
                        <Badge pill>{randomMessagesLength}</Badge> : null}

                </ListGroupItem>
                <ListGroupItem className="justify-content-between">
                    <NavLink exact to={Routes.FRONTEND_TEAM}>#fronted-team</NavLink>

                    {frontendMessagesLength !== null ?
                        <Badge pill>{frontendMessagesLength}</Badge> : null}

                </ListGroupItem>
                <ListGroupItem className="justify-content-between">
                    <NavLink exact to={Routes.TODAY_LEARNED}>#today-i-learned</NavLink>

                    {todayLearnedMessagesLength !== null ?
                        <Badge pill>{todayLearnedMessagesLength}</Badge> : null}

                </ListGroupItem>
            </ListGroup>


        );
    }
}

const ChannelContainer = withRouter(connect(({message}) => ({
        redux_messages: message.messages,
    }),
    {saga_getMessages: MessageActions.action_getMessagesRequest}
)(SideBar));

export {ChannelContainer as SideBar};
