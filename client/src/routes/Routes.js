const Routes = {
    GENERAL: '/general',
    FRONTEND_TEAM: '/frontendTeam',
    TODAY_LEARNED: '/todayLearned',
    RANDOM: '/random',
};

export {Routes};