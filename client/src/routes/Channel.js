import React from "react";
import {withRouter} from "react-router-dom";
import {Button, Container, InputGroup, InputGroupAddon, Input, ListGroup, ListGroupItem} from 'reactstrap'
import {connect} from "react-redux";
import io from 'socket.io-client';
import moment from 'moment';
import {Routes} from '../routes'
import * as MessageActions from '../store/actions/messageActions'

// CHALK ---------------------------------
const log = console.log;
const chalk = require('chalk');

// const error = chalk.bold.red;
// const warning = chalk.keyword('orange');
//-----------------------------------------

const socket = io();
socket.on('connection', () => {
    console.log("Connected to server", new Date());
});

socket.on('disconnect', () => {
    console.log('Disconnected from server', new Date());
});

socket.on('newMessage', (email) => {
    console.log("Sending message", email);
});

class Channel extends React.Component {

    state = {
        messageText: null,
        generalChannel: [],
        randomChannel: [],
        frontedTeam: [],
        todayLearner: [],
    };

    deleteItem(id) {
        // copy current list of items
        const list = [...this.state.list];
        // filter out the item being deleted
        const updatedList = list.filter(item => item.id !== id);

        this.setState({list: updatedList});
    }

    addItem() {
        // create a new item
        const newItem = {
            id: 1 + Math.random(),
            value: this.state.newItem.slice()
        };

        // copy current list of items
        const list = [...this.state.list];

        // add the new item to the list
        list.push(newItem);

        // update state with new list, reset the new item input
        this.setState({
            list,
            newItem: ""
        });
    }

    componentDidMount() {

        const items = localStorage.getItem('messagesByChannels');
        console.log("Items [Channel.js]: ", JSON.parse(items));

        this.props.saga_getMessages();

        const messagesFromDB = this.props.redux_messages;
        this.setState({
            generalChannel: messagesFromDB.generalMessages,
            randomChannel: messagesFromDB.randomMessages,
            frontedTeam: messagesFromDB.frontendMessages,
            todayLearner: messagesFromDB.todayLearnedMessages,
        });

        socket.on("newMessage", (message) => {
            this.toWhatChannelAddMessage(message.channel, message);
        });

    }

    toWhatChannelAddMessage = (parameter, message) => {
        if (parameter === "general") {
            this.setState(state => ({
                generalChannel: [...state.generalChannel, message]
            }));
        }

        if (parameter === "random") {
            this.setState(state => ({
                randomChannel: [...state.randomChannel, message]
            }));
        }

        if (parameter === "frontendTeam") {
            this.setState(state => ({
                frontedTeam: [...state.frontedTeam, message]
            }));
        }

        if (parameter === "todayLearned") {
            this.setState(state => ({
                todayLearner: [...state.todayLearner, message]
            }));
        }
    };

    componentWillUnmount() {

        const dataToBeSaved = {
            generalChannel: this.state.generalChannel,
            randomChannel: this.state.randomChannel,
            frontedTeam: this.state.frontedTeam,
            todayLearner: this.state.todayLearner,
        };

        localStorage.setItem('messagesByChannels', JSON.stringify(dataToBeSaved));
        console.log("UNMOUNT!!!!")
    }


    onChange = (type, value) => {
        this.setState({[type]: value})
    };

    sendMessage = () => {
        const {nickname} = this.props;
        const time = moment();

        const message = {
            time: time.format("YY.M.D H:mm:ss"),
            from: nickname,
            channel: this.props.match.params.id,
            text: this.state.messageText
        };

        this.toWhatChannelAddMessage(this.props.match.params.id, message);
        socket.emit('clientCreateMessage', message);
    };

    // componentWillMount() {
    //     this.props.saga_getMessages(this.props.match.params.id);
    // }


    getMessages = (channelName) => {
        const {nickname} = this.props;

        socket.on('newMessage', {
            from: nickname,
            channel: this.props.match.params.id,
            text: this.state.messageText
        });
    };


    render() {


        const routeID = this.props.match.params.id;

        let resultList = null;

        if (routeID === "general") {
            resultList = (
                <ListGroup>
                    {this.state.generalChannel.map((item) => (
                        <ListGroupItem color="warning"
                                       onMouseEnter={(event) => {
                                           log("Mouse event:", event);
                                       }}
                                       onMouseOver={(event) => {
                                           log("Mouse over:", event);
                                       }}>
                            From:<b>{item.from}</b> Time:<i>{item.time}</i>
                            <br/>{item.text}
                        </ListGroupItem>
                    ))}
                </ListGroup>)
        }

        if (routeID === "random") {
            resultList = (
                <ListGroup>
                    {this.state.randomChannel.map((item, index) => (
                        <ListGroupItem color="warning" id={index}
                                       onMouseEnter={(event) => {
                                           log("Mouse event:", event);
                                       }}
                                       onMouseOver={(event) => {
                                           log("Mouse over:", event);
                                       }}>
                            From:<b>{item.from}</b> Time:<i>{item.time}</i>
                            <br/>{item.text}
                        </ListGroupItem>
                    ))}
                </ListGroup>)
        }

        if (routeID === "frontendTeam") {
            resultList = (
                <ListGroup>
                    {this.state.frontedTeam.map((item) => (
                        <ListGroupItem color="warning"
                                       onMouseEnter={(event) => {
                                           log("Mouse event:", event);
                                       }}
                                       onMouseOver={(event) => {
                                           log("Mouse over:", event);
                                       }}>
                            From:<b>{item.from}</b> Time:<i>{item.time}</i>
                            <br/>{item.text}
                        </ListGroupItem>
                    ))}
                </ListGroup>)
        }

        if (routeID === "todayLearned") {
            resultList = (
                <ListGroup>
                    {this.state.todayLearner.map((item) => (
                        <ListGroupItem color="warning"
                                       onMouseEnter={(event) => {
                                           log("Mouse event:", event);
                                       }}
                                       onMouseOver={(event) => {
                                           log("Mouse over:", event);
                                       }}>
                            From:<b>{item.from}</b> Time:<i>{item.time}</i>
                            <br/>{item.text}
                        </ListGroupItem>
                    ))}
                </ListGroup>)
        }


        return (
            <Container>
                <h3>ID: {routeID}</h3>
                {resultList}
                <InputGroup>
                    <Input onChange={(event) => this.onChange("messageText", event.target.value)}/>
                    <InputGroupAddon addonType="append">
                        <Button color="success" onClick={this.sendMessage}>Send message</Button>
                    </InputGroupAddon>
                </InputGroup>
            </Container>
        );
    }
}

const ChannelContainer = withRouter(connect(({message}) => ({
        redux_messages: message.messages,
    }),
    {saga_getMessages: MessageActions.action_getMessagesRequest}
)(Channel));

export {ChannelContainer as Channel};
