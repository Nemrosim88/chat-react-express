import {Routes} from "./Routes";
import {FrontendTeamScreen, GeneralScreen, RandomScreen, TodayLearnedScreen} from "../components/pages";

const ChannelsRoutes = [
    {
        path: Routes.GENERAL,
        exact: true,
        component: GeneralScreen,
        // breadcrumb: i18n.t('dashboard__page_title')
    },
    {
        path: Routes.FRONTEND_TEAM,
        exact: true,
        component: FrontendTeamScreen,
        // breadcrumb: i18n.t('teams__page_title')
    },
    {
        path: Routes.RANDOM,
        exact: true,
        component: RandomScreen,
        // breadcrumb: i18n.t('teams__page_title')
    },
    {
        path: Routes.TODAY_LEARNED,
        exact: true,
        component: TodayLearnedScreen,
        // breadcrumb: i18n.t('teams__page_title')
    },
];

export {ChannelsRoutes};