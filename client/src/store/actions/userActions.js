import * as Types from "../types";

// ==================================================================
// ========================    ADD_USER    ==========================

export const action_addUserRequest = ({email, password, nickname, gender, photo}) => ({
    type: Types.ADD_USER_REQUEST, email, password, nickname, gender, photo
});
export const action_addUserSuccess = ({data}) => ({type: Types.ADD_USER_SUCCESS, data});
export const action_addUserFailure = ({error}) => ({type: Types.ADD_USER_FAILURE, error});
