import * as Types from "../types";

// ==================================================================
// ========================    LOGIN    =============================

export const action_loginRequest = ({email, password,}) => ({
    type: Types.LOGIN_REQUEST, email, password,
});
export const action_loginRequestSuccess = ({data}) => ({type: Types.LOGIN_SUCCESS, data});
export const action_loginRequestFailure = ({error}) => ({type: Types.LOGIN_FAILURE, error});

// ==================================================================
// ========================    LOGOUT    ============================

export const action_logoutRequest = ({token, tokenKey}) => ({
    type: Types.LOGOUT_REQUEST, token, tokenKey
});
export const action_logoutRequestSuccess = ({data}) => ({type: Types.LOGOUT_SUCCESS, data});
export const action_logoutRequestFailure = ({error}) => ({type: Types.LOGOUT_FAILURE, error});