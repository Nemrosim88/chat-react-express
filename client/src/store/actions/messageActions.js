import * as Types from "../types";

// -------------------    LOGIN    ---------------------------------
export const action_getMessagesRequest = () => ({
    type: Types.GET_MESSAGES_REQUEST,
});
export const action_getMessagesSuccess = ({data}) => ({type: Types.GET_MESSAGES_SUCCESS, data});
export const action_getMessagesFailure = ({error}) => ({type: Types.GET_MESSAGES_FAILURE, error});