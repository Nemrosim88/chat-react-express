import * as Types from "../types/index";

const initialState = {
    downloading: false,
    downloaded: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.ADD_USER_REQUEST:
            return {
                ...state,
                downloading: true,
                downloaded: false,
            };
        case Types.ADD_USER_SUCCESS:
            return {
                ...state,
                downloading: false,
                downloaded: true,
            };
        case Types.ADD_USER_FAILURE:
            return {
                ...initialState
            };
        default:
            return state;
    }
};