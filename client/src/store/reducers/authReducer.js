import * as Types from "../types/index";

const initialState = {
    logging: false,
    logged: false,
    email: null,
    nickname: null,
    token: null,
    tokenKey: null,
    error: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.LOGIN_REQUEST:
            return {
                ...state,
                logging: true,
            };
        case Types.LOGIN_SUCCESS:
            const {email, token, tokenKey,nickname} = action.data;
            return {
                ...state,
                logging: false,
                logged: true,
                email: email,
                token: token,
                tokenKey: tokenKey,
                nickname:nickname,
            };
        case Types.LOGIN_FAILURE:
            return {
                ...state,
                logging: false,
                logged: false,
                email: null,
                token: null,
                tokenKey: null,
                nickname: null,
                error: action.error
            };
        // ===================================================
        // ============      LOGOUT      =====================
        // ===================================================
        case Types.LOGOUT_REQUEST:
            return {
                ...state,
                logging: true,
            };
        case Types.LOGOUT_SUCCESS:
            return {
                ...initialState,
            };
        case Types.LOGOUT_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        default:
            return state;
    }
};