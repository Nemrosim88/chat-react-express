import * as Type from "../types/index";
import * as Types from "../types";

const initialState = {
    loading: false,
    messages: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case Types.GET_MESSAGES_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case Type.GET_MESSAGES_SUCCESS:
            return {
                ...state,
                loading: false,
                messages: action.data
            };
        case Types.GET_MESSAGES_FAILURE:
            return {
                ...state,
                loading: false,
            };
        default:
            return state;
    }
};