import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';

import auth from './authReducer';
import message from './messageReducer';
import user from './userReducer';

export default (history) => combineReducers({
    router: connectRouter(history),
    auth,
    message,
    user,
});