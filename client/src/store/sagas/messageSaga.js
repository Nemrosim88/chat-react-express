import {call, fork, put, takeLatest} from 'redux-saga/effects';

import {messageService} from "../../domain";
import * as Types from "../types";
import * as MessageActions from "../actions/messageActions";

function* saga_messageRequest() {
    try {
        console.log("SAGA");
        const data = yield call(() => messageService.getMessagesRequest());
        console.log("Message data:", data);
        yield put(MessageActions.action_getMessagesSuccess({data}));
    } catch (error) {
        yield put(MessageActions.action_getMessagesFailure({error}));
    }
}

function* messageListener() {
    yield takeLatest(Types.GET_MESSAGES_REQUEST, saga_messageRequest);
    // yield takeLatest(Types.ANOTHER_ACTION, anotherFunction);
}

export default function* messageSaga() {
    yield fork(messageListener);
}