import {call, fork, put, takeLatest} from 'redux-saga/effects';

import {authService} from "../../domain";
import * as Types from "../types";
import * as AuthActions from "../actions/authActions";

function* saga_loginRequest({email, password}) {

    try {
        const data = yield call(() => authService.login({email, password}));
        console.log("DATA:", data);
        if (data.error === undefined || data.error === null) {
            yield put(AuthActions.action_loginRequestSuccess({data}));
        } else {
            const error = data.error;
            yield put(AuthActions.action_loginRequestFailure({error}));
        }
    } catch (error) {
        yield put(AuthActions.action_loginRequestFailure({error}));
    }
}

function* saga_logoutRequest({token, tokenKey}) {

    try {
        const data = yield call(() => authService.logoutService({token, tokenKey}));
        console.log("STATUS", data.status);

        yield put(AuthActions.action_logoutRequestSuccess({data}));

    } catch (error) {
        yield put(AuthActions.action_logoutRequestFailure({error}));
    }
}

function* authListener() {
    yield takeLatest(Types.LOGIN_REQUEST, saga_loginRequest);
    yield takeLatest(Types.LOGOUT_REQUEST, saga_logoutRequest);
    // yield takeLatest(Types.ANOTHER_TYPE, anotherMethod);
}

export default function* authSaga() {
    yield fork(authListener);
}