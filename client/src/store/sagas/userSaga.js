import {call, fork, put, takeLatest} from 'redux-saga/effects';

import {userService} from "../../domain";
import * as Types from "../types";
import * as UserActions from "../actions/userActions";

function* saga_addUserRequest({email, password, nickname, gender, photo}) {

    try {
        const data = yield call(() => userService.addUserService({email, password, nickname, gender, photo}));
        yield put(UserActions.action_addUserSuccess({data}));
    } catch (error) {
        yield put(UserActions.action_addUserFailure({error}));
    }
}


function* userListener() {
    yield takeLatest(Types.ADD_USER_REQUEST, saga_addUserRequest);
}

export default function* userSaga() {
    yield fork(userListener);
}