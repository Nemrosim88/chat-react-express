import {all} from 'redux-saga/effects';

import authSaga from "./authSaga";
import messageSaga from "./messageSaga";
import userSaga from "./userSaga";

export default function* root() {
    yield all([
        authSaga(),
        messageSaga(),
        userSaga(),
    ]);
};
