import React, {Component} from "react";
import connect from "react-redux/es/connect/connect";
import {Container,} from 'reactstrap';

import {AppNav} from './components/Nav/AppNav'
import {MainForm} from './components/MainForm';
import './App.css'
import NotLoggedForm from "./components/NotLoggedForm";

class App extends Component {

    render() {

        const {logged} = this.props;

        return (
                <Container>
                    <AppNav/>
                    {logged ? <MainForm/> : <NotLoggedForm/>}
                </Container>

        );
    }
}

const AppContainer = connect(({auth}) => ({
        logging: auth.logging,
        logged: auth.logged,
    }),
    {}
)(App);

export {AppContainer as App};
