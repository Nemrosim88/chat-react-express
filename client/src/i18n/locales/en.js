// ENGLISH
export default {
    login: 'Login',
    email: 'Email',
    password: 'Password',

    // ADD_USER
    addUser__registrationForm: 'Registration form',
    addUser__email_placeholder: 'Email',
    addUser__password_placeholder: 'Password',
    addUser__secondPassword_placeholder: 'Repeat password',
    addUser__nickname_placeholder: 'Nickname',
    addUser__sex_placeholder: 'Sex',
    addUser__photo_placeholder: 'Photo',
    addUser__inDevelopment: 'In development',
    addUser__sex_male: 'Male',
    addUser__sex_female: 'Female',
    addUser__registrationButton: 'Register',
    addUser__cancelButton: 'Cancel',


};
