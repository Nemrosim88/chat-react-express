import ru from './ru';
import en from './en';
import uk from './uk';

export default {
    ru,
    en,
    uk,
};
