// RUSSIAN
export default {
    login: 'Логин',
    email: 'Почта',
    password: 'Пароль',

    // ADD_USER
    addUser__registrationForm: 'Форма регистрации',
    addUser__email_placeholder: 'Почта',
    addUser__password_placeholder: 'Пароль',
    addUser__secondPassword_placeholder: 'Повторите пароль',
    addUser__nickname_placeholder: 'Никнейм',
    addUser__sex_placeholder: 'Пол',
    addUser__photo_placeholder: 'Фото',
    addUser__inDevelopment: 'В разработке',
    addUser__sex_male: 'Мужчина',
    addUser__sex_female: 'Женщина',
    addUser__registrationButton: 'Регистрация',
    addUser__cancelButton: 'Отмена',
};
