//  ----------- MODULES -------------
import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import 'bootstrap/dist/css/bootstrap.min.css';
// ----------------------------------
//  ----------- COMPONENTS ----------
import {History, Store} from "./store";
import {App} from "./App";
// ----------------------------------

global.__DEV__ = process.env.NODE_ENV === 'development';

const Root = () => (
    <Provider store={Store}>
        <ConnectedRouter history={History}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(<Root/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA

